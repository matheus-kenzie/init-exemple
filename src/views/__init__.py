from .home_view import home_view
from .info_view import info_view

def init_app(app):
    home_view(app)
    info_view(app)
    return app