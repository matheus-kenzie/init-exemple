from flask import Flask
from src import views

app = Flask(__name__)

views.init_app(app)